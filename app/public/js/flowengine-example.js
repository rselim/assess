/**
 * Created by ramyselim on 5/17/15.
 */

var flowEngineExample = {
    input:"//Object passed as param to the rule function\n{\n\tcolor:'red'\n}",
    data:[
        {
            title: 'Rule 1',
            body: "function(obj){\n\tif(obj.color == 'red')\n\t\treturn true;\n\telse\n\t\treturn false;\n}",
            passId: '2',
            failId: '3'
        },
        {
            title: 'Rule 2',
            body: "function(obj){\n\tif(obj.color == 'blue')\n\t\treturn true;\n\telse\n\t\treturn false;\n}",
            passId: '4',
            failId: '5'
        },
        {
            title: 'Rule 3',
            body: "function(obj){\n\treturn !obj;\n}",
            passId: '',
            failId: ''
        },
        {
            title: 'Rule 4',
            body: "function(obj){\n\treturn false;\n}",
            passId: '',
            failId: ''
        },
        {
            title: 'Rule 5',
            body: "function(obj){\n\treturn true;\n}",
            passId: '',
            failId: ''
        }
    ]
};
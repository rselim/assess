/**
 * Created by ramyselim on 5/17/15.
 */

var flowEngine = {
    current_id:0,

    /**
     * Query selector similr to jQuery $("#example");
     *
     * @param string
     * @returns {HTMLElement}
     */
    qs:function(string){
        return document.querySelector(string);
    },

    /**
     * initialize the app with predefined state
     */
    initExample:function(){
        this.qs("#"+flowEngineHelper.execution_object).value = flowEngineExample.input;
        for(var i = 0;i < flowEngineExample.data.length; i++) {
            this.addRule();

            this.qs("#"+flowEngineHelper.rule_id_prefix+this.current_id+"-title").value = flowEngineExample.data[i].title;
            this.qs("#"+flowEngineHelper.rule_id_prefix+this.current_id+"-body").value = flowEngineExample.data[i].body;
            this.qs("#"+flowEngineHelper.rule_id_prefix+this.current_id+"-passed").value = flowEngineExample.data[i].passId;
            this.qs("#"+flowEngineHelper.rule_id_prefix+this.current_id+"-failed").value = flowEngineExample.data[i].failId;

            var event = new Event('blur');
            this.qs("#"+flowEngineHelper.rule_id_prefix+this.current_id+"-body").dispatchEvent(event);
        }

        window.scrollTo(0, 0);
    },

    /**
     * initialize the app with clear state
     */
    init:function(){
        this.current_id = 0;
        this.qs("#"+flowEngineHelper.result_div_id).innerHTML = "";
        this.qs("#"+flowEngineHelper.rule_div_id).innerHTML = "";
        this.qs("#"+flowEngineHelper.execution_object).value = "";
    },

    /**
     * Generate single rule template to be
     * appended later to the container
     *
     * @returns {HTMLElement}
     */
    getCurrentRuleElement:function(){
        var single_rule = document.createElement("div");
        single_rule.id = flowEngineHelper.rule_id_prefix.toString()+(this.current_id+1);
        single_rule.className = flowEngineHelper.rule_class_name+" fadeIn";
        single_rule.innerHTML = flowEngineHelper.getRuleTemplate(this.current_id+1);

        return single_rule;
    },

    /**
     * Append the current rule element to
     * rules container
     */
    addRule:function(){
        var container = this.qs("#"+flowEngineHelper.rule_div_id);
        container.appendChild(this.getCurrentRuleElement());
        this.current_id++;

        this.qs("#"+flowEngineHelper.rule_id_prefix+this.current_id+"-title").focus();

        //Use code hightlighter if the library is found
        if(typeof hljs !== "undefined") {
            var block = this.qs("#" + flowEngineHelper.rule_id_prefix + this.current_id);
            var codeBlock = block.getElementsByTagName('pre')[0];
            var bodyBlock = block.getElementsByTagName('textarea')[0];

            var _this = this;
            var currentId = _this.current_id;

            bodyBlock.addEventListener("blur", function (event) {
                _this.qs("#" + flowEngineHelper.rule_id_prefix + currentId).className += " code-ready";
                codeBlock.getElementsByTagName('code')[0].innerHTML = _this.qs("#"+flowEngineHelper.rule_id_prefix+currentId+"-body").value;
                hljs.configure({
                    tabReplace: '    '
                });
                hljs.highlightBlock(codeBlock);
            }, true);

            codeBlock.addEventListener("click", function (event) {
                var reg = new RegExp('(\\s|^)code-ready(\\s|$)');
                _this.qs("#" + flowEngineHelper.rule_id_prefix + currentId).className = _this.qs("#" + flowEngineHelper.rule_id_prefix + currentId).className.replace(reg,'');
                bodyBlock.focus();
            }, true);
        }
    },

    /**
     * Get result template withers its true and false
     *
     * @param state
     * @returns {HTMLElement}
     */
    getCurrentResultElement:function(state){
        var icon = state.state ? "<i class='glyphicon glyphicon-ok'></i>" : "<i class='glyphicon glyphicon-remove'></i>";
        var single_result = document.createElement("div");
        single_result.className = state.state? flowEngineHelper.result_class_name+" fadeIn" : flowEngineHelper.result_class_name + " error fadeIn";
        single_result.innerHTML = "<span>"+state.title+icon+"</span>";

        return single_result;
    },

    /**
     * add result template to the container
     *
     * @param state
     */
    addResult:function(state){
        var container = this.qs("#"+flowEngineHelper.result_div_id);
        container.appendChild(this.getCurrentResultElement(state));
    },

    /**
     * Check if given array has unique values
     *
     * @param array
     * @returns {boolean}
     */
    isArrayIsUnique: function(array){
        array = array.sort();
        for ( var i = 1; i < array.length; i++ ){
            if(array[i-1] == array[i])
                return false; //duplicate value
        }
        return true;
    },
    /**
     * Validate all rules and check
     * that the engine requirment is set
     *
     * @returns {boolean}
     */
    hasErrors:function(){
        var title,body,pass,fail,ids;
        ids = [];
        if(this.current_id < 1){
            return "Error there is no rules.. add some.";
        }
        //check that title and body have input on all rules
        for(var i = 0;i<this.current_id;i++){
            title = this.qs("#"+flowEngineHelper.rule_id_prefix+(i+1)+"-title").value;
            body = this.qs("#"+flowEngineHelper.rule_id_prefix+(i+1)+"-body").value;
            pass = this.qs("#"+flowEngineHelper.rule_id_prefix+(i+1)+"-passed").value;
            fail = this.qs("#"+flowEngineHelper.rule_id_prefix+(i+1)+"-failed").value;

            if(title.trim() == "")
                return "Error ( rule id "+(i+1)+" ) title cant be blank";
            if(body.trim() == "")
                return "Error ( rule id "+(i+1)+" ) body cant be blank";
            if(pass == 1|| fail == 1)
                return "Error ( rule id "+(i+1)+" ) first rule id cant be in pass/fail id";

            if(pass.trim() != "")
                ids.push(pass.trim());
            if(fail.trim() != "")
                ids.push(fail.trim());
        }

        if(this.isArrayIsUnique(ids))
            return false; // No errors
        else
            return "Error it seems that one of the rule ids is duplicated with different pass_id / fail_id";
    },

    /**
     * Show error if any from the validation
     * it will use toast jquery library if found
     *
     * @param error
     */
    showError:function(error){
        if(typeof toastr !== "undefined"){
            toastr.error(error);
        }else{
            alert(error);
        }
    },

    /**
     * The method contains try catch because the user can write
     * broken code so we make sure the app dont crash in this case and
     * show him what he wrote wrong
     *
     * @param id
     * @returns {*}
     */
    executeSingleRule:function(id){
        try{
            var title = this.qs("#"+flowEngineHelper.rule_id_prefix+id+"-title").value;
            var body = this.qs("#"+flowEngineHelper.rule_id_prefix+id+"-body").value;
            var passId = this.qs("#"+flowEngineHelper.rule_id_prefix+id+"-passed").value;
            var failId = this.qs("#"+flowEngineHelper.rule_id_prefix+id+"-failed").value;

            var userRule = "var userFunc = "+body;
            eval(userRule);
            var result = eval("userFunc("+this.qs("#"+flowEngineHelper.execution_object).value+")");

            return {
                title:title,
                passId:passId,
                failId:failId,
                state:result
            }
        }catch (e){
            this.showError("Error ( rule Id "+id+" )   "+e.message);
            return {
                title:"",
                passId:"",
                failId:"",
                state:false
            }
        }
    },
    /**
     * execute all rules
     */
    execute:function(){
        this.qs("#"+flowEngineHelper.result_div_id).innerHTML = "";

        var error = this.hasErrors();
        if(!error){
            var rule = 1;
            while(true){
                var result = this.executeSingleRule(rule);
                this.addResult(result);

                if(result.passId == "" || result.failId == "")
                    break;

                if(result.state){
                    rule = result.passId;
                }else{
                    rule = result.failId;
                }
            }
        }else{
            this.showError(error);
        }
    }

};




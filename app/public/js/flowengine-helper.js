/**
 * Created by ramyselim on 5/17/15.
 */

var flowEngineHelper = {
    rule_div_id:"rules-container",
    rule_id_prefix:"rule-",
    rule_class_name:"single-rule",
    result_div_id:"result-container",
    result_class_name:"hexagon",
    execution_object:"execute-object",
    getRuleTemplate:function(id){
        var str_var="";
        str_var += "                    <div class=\"rule-id-circle\">"+id+"<\/div>";
        str_var += "                    <form class=\"form-horizontal\">";
        str_var += "                        <div class=\"form-group\">";
        str_var += "                            <label for="+this.rule_id_prefix+id+"-title class=\"text-left col-sm-2 control-label\">Title<\/label>";
        str_var += "                            <div class=\"col-sm-10\">";
        str_var += "                                <input type=\"text\" class=\"form-control\" id="+this.rule_id_prefix+id+"-title placeholder=\"Rule title\">";
        str_var += "                            <\/div>";
        str_var += "                        <\/div>";
        str_var += "                        <div class=\"form-group\">";
        str_var += "                            <label for="+this.rule_id_prefix+id+"-body class=\"text-left col-sm-2 control-label\">Body<\/label>";
        str_var += "                            <div class=\"col-sm-10\">";
        str_var += "                                <textarea class=\"form-control\" rows=\"6\" id="+this.rule_id_prefix+id+"-body placeholder=\"Rule body\"><\/textarea>";
        str_var += "                                <pre><code class='code'><\/code><\/pre>";
        str_var += "                            <\/div>";
        str_var += "                        <\/div>";
        str_var += "                        <div class=\"form-group\">";
        str_var += "                            <label for="+this.rule_id_prefix+id+"-passed class=\"text-left col-sm-2 control-label\">Passed Id<\/label>";
        str_var += "                            <div class=\"col-sm-10\">";
        str_var += "                                <input type=\"text\" class=\"form-control\" id="+this.rule_id_prefix+id+"-passed placeholder=\"Passed Rule ID\">";
        str_var += "                            <\/div>";
        str_var += "                        <\/div>";
        str_var += "                        <div class=\"form-group\">";
        str_var += "                            <label for="+this.rule_id_prefix+id+"-failed class=\"text-left col-sm-2 control-label\">Failed Id<\/label>";
        str_var += "                            <div class=\"col-sm-10\">";
        str_var += "                                <input type=\"text\" class=\"form-control\" id="+this.rule_id_prefix+id+"-failed placeholder=\"Failed Rule ID\">";
        str_var += "                            <\/div>";
        str_var += "                        <\/div>";
        str_var += "                    <\/form>";

        return str_var;
    }
};


